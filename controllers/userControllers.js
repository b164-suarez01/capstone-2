const User = require("../models/User");
//for add to cart and checkout
const Product = require("../models/Product");


//encrypted password
const bcrypt = require('bcrypt');
const auth = require("../auth");


//Register User
module.exports.registerUser = (reqBody) => {

	//Creates a new User object
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		//ung 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password,10)
	})
	//Saves the created object to our darabase
	return newUser.save().then((user,error) => {
		//if user registration failed
		if(error){
			return false;
		} else {
			//user registration is success
			return true;
		}
	})
}

//Login User
module.exports.loginUser = (reqBody) => {
	//findOne - it will return the first record in the collection that matches the search criteria. Kasi pag find ginamit, mas flexible siya at irereturn niya lahat ng magmamatch sa kanya

	return User.findOne({email:reqBody.email}).then(result =>{
		//User does not exist
		if (result == null){
			return false;
		}else {
			//User exists

			//the compareSync method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false"
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			//if password match
			if(isPasswordCorrect){
				//Generate n access token
				return{ accessToken : auth.createAccessToken(result.toObject())}
			} else {
				//Password does not match
				return false;
			}
		};
	});
};


//Add to Cart by User
module.exports.addToCart = async(data) => {
//Add the product
 let isUserUpdated =await User.findById(data.userId).then (user =>{

//Push the course Id to enrollments property
user.addToCartOrders.push({productId: data.productId});

//Save

return user.save().then((user,error) =>{
	if(error){
		return false;
		} else{
			return true;
		}
})
});
	
	let isProductUpdated = await Product.findById(data.productId).then(product => {
		//add the userId in the product's database (addToCart)
		product.addToCartBy.push({userId:data.userId});

		return product.save().then((product,error) => {
			if(error){
				return false;
			} else{
				return true;
			}
		})
	});

//Validation
	if(isUserUpdated && isProductUpdated) {
		return true;
	}else {
		return false;
	}

};


//CheckOut by User
module.exports.checkOut = async(data) => {
//Add the product

 let isUserUpdated =await User.findById(data.userId).then (user =>{
//Push the course Id to enrollments property
user.checkOutOrders.push(
	{productId: data.productId,
	 quantity: data.quantity,
	 paymentType: data.paymentType
	});
// {user.transactions.push(
// 	{userId: data.userId,
// 	 transactionStatus: data.paymentStatus}
// 	)};

//Save

return user.save().then((user,error) =>{
	if(error){
		return false;
		} else{
			return true;
		}
})
});
	let isProductUpdated = await Product.findById(data.productId).then(product => {
		//add the userId in the product's database (checkOut)
		product.checkOutBy.push(
			{userId: data.userId,
			quantity: data.quantity}
	);
		return product.save().then((product,error) => {
			if(error){
				return false;
			} else{
				return true;
			}
		})
	});

//Validation
	if(isUserUpdated && isProductUpdated) {
		return true;
	}else {
		return false;
	}
};



module.exports.getAllUserOrder = function(userId) {
	
 return(User.find({_id:userId})).then(data => {
 		let userOrder=[];
 		if(data[0].addToCartOrders!=0 && data[0].checkOutOrders!=0){
 			userOrder.push(data[0].addToCartOrders,data[0].checkOutOrders)
 			return userOrder;
 		}
 		else if (data[0].addToCartOrders==0 && data[0].checkOutOrders!=0){
 			userOrder.push(data[0].checkOutOrders)
			return userOrder;
 		}
 		else if (data[0].addToCartOrders!=0 && data[0].checkOutOrders==0){
 			userOrder.push(data[0].addToCartOrders)
			return userOrder;
 		} else {
 	
 		} 		
})}


module.exports.getAllOrders = function(result) {
	
 return(User.find({})).then(data => {
 	let length = data.length;
 	let allOrder = [];
 		for(let i=0; i<length;i++){
 			if(data[i].checkOutOrders != 0){
 			allOrder.push(data[i].email,data[i].mobileNo,data[i].checkOutOrders)
 			} else{}
		}
		return allOrder;
})
}
