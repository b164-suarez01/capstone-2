const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const port = 5000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//http://localhost:5000/api/users
app.use("/api/users",userRoutes);

app.use("/api/products", productRoutes);

mongoose.connect('mongodb+srv://bssuarez07:Crusaders07@cluster00.qysuq.mongodb.net/batch164_capstone2?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology:true
})
mongoose.connection.once('open', () => console.log(`Now connected to MongoDB Atlas`));
app.listen(port, () => {
	console.log(`API is now online on port ${port}`)
})