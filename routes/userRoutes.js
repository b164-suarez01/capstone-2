const express = require("express");
const router = express.Router();
const auth = require("../auth");

const UserController = require("../controllers/userControllers");

//Registration for new user
router.post("/register", (req,res) =>{
	UserController.registerUser(req.body).then(result => res.send(result));
})

//User Authentication (login)
router.post("/login", (req,res) => {
	UserController.loginUser(req.body).then (result => res.send(result));
});


/*Add to cart by a user only*/
router.post("/addToCart",auth.verify,(req,res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	} 
	if(data.isAdmin){
		res.send(false)
	} else {
		UserController.addToCart(data).then(result => res.send(result));
	}
});

/*Checkout a product by a user only*/

router.post("/checkOut",auth.verify,(req,res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		quantity: req.body.quantity,
		paymentType: req.body.paymentType
	} 
	if(data.isAdmin){
		res.send(false)
	} else {
		console.log(data);
		UserController.checkOut(data).then(result => res.send(result));
	}
});



/*Retrieving Orders by User*/

router.get("/userOrder",auth.verify,(req,res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	} 
	if(data.isAdmin){
		res.send(false)
	} else {
		UserController.getAllUserOrder(data.userId).then(result => res.send(result));
	} 
});

//Retrieve All CheckOut Orders
router.get("/allOrders",auth.verify,(req,res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	} 
	if(data.isAdmin){
		UserController.getAllOrders(data).then(result => res.send(result));
	
	} else {
		res.send(false)
	} 
});




module.exports = router;